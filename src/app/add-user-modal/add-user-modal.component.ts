import { User } from './../interfases/user';
import { DataService } from './../services/data.service';
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})


export class AddUserModalComponent {

  @Output() closeEvent = new EventEmitter();

  constructor(private DataService: DataService) { }

  public userName: string;
  public email: string;
  public age: number;

  public closeModal(): void {
    this.closeEvent.next()
  }


  public saveUser(): void {
    if (this.userName && this.age && this.email) {

      const user: User = {
        id: Math.random().toString(),
        name: this.userName,
        email: this.email,
        age: this.age,
        avatar: 'assets/images.jpg'
      };

for( const key in user) {
  console.log(key +' = ' + user [key]);
}






      this.DataService.newUserData = user;
      this.closeModal();
    }
  }

}
