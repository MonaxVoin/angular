import { Injectable } from "@angular/core";
import { BackendusersData, User } from './../interfases/user';
import { HttpClient } from '@angular/common/http';


@Injectable({ providedIn: 'root' })


export class DataService {

public newUserData: User;


  constructor(private http: HttpClient) { }





  public loadUserList(): Promise<BackendusersData> {
    return this.http.get<BackendusersData>('https://randomuser.me/api?results=10')
    .toPromise();

  }


  // public addUser(user: User): void {
  //   this.users.push(user);
  // }

  // public deleteUser(id: number): void {
  //   this.users = this.users.filter(user => user.id !== id);
  // }


  // public createNewUser(name: string, age: number, email: string): User {

  //   const ids = this.users.map(item => item.id);
  //   const maxId = ids.length > 0 ? Math.max(...ids) : -1; // 1.2.3.
  //   const newId = maxId + 1;


  //   const user: User = {
  //     id: newId,
  //     age,
  //     email,
  //     name
  //   };

  //   return user;
  // }

}
