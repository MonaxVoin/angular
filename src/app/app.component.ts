import { DataService } from './services/data.service';
import { Component, OnInit } from '@angular/core';
import { User, Result } from './interfases/user';

@Component({//декоратор
  selector: 'app-root',//название тега
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {

  // title = 'firstApp';
  //static - переменная на уровне класса
  //private-доступ только внутри ts
  //public-доступна в html


  public users: User[] = [];
  public isModalOpened: boolean;

  constructor(public dataService: DataService) {

  }

  ngOnInit(): void {
    console.log('App component inited');
    this.dataService.loadUserList().then(data => {
      this.users = data.results.map(item => this.convertToUsres(item));
    });
  }


  public addUser(): void {
    this.isModalOpened = true;


    // const newUser = this.dataService.createNewUser('My User', 205, 'test@test.ru');

    // this.dataService.addUser(newUser);
  }



  public closeModal(): void {

    if (this.dataService.newUserData) {
      this.users.push(this.dataService.newUserData);
      this.dataService.newUserData = null;
    }

    this.isModalOpened = false;



  }

  public deleteUser(id: string): void {
    this.users = this.users.filter(item => item.id !== id);


    // const user = this.ages.find(item=>item.id === id)
    //   const index = this.ages.indexOf(user);
    //   this.ages.splice(index, 1);
    // this.dataService.deleteUser(id);
  }



  private convertToUsres(userData: Result): User {
    const { email, dob, picture } = userData;


    const id = userData.login.uuid;
    const age = dob.age;
    // const email = userData.email;
    const name = userData.name.first + ' ' + userData.name.last;
    const avatar = picture.medium;


    const user: User = {
      id,
      age,
      email,
      name,
      avatar
    };
    return user;
  }
  // public count = 1;


  // public addCounter() {
  //   this.count++;
  // }
}


