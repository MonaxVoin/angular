import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';
import { DataService } from './services/data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppCounterPipe } from './pipes/counter.pipe';
import { UserCardComponent } from './user-card/user-card.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    AppCounterPipe,
    // appCounterPipe,
    UserCardComponent,
    AddUserModalComponent
  ],
  imports: [//только модули
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
  ],// сервисы


  bootstrap: [AppComponent]// стартовый компонент
})
export class AppModule { }
