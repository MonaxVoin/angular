import{Component} from '@angular/core';



@Component({
    selector: 'app-my-btn1',
    templateUrl:'my-btn.component.html',
    styleUrls:['my-btn1.component.cssc']
})

export class MyBtnComponent {
    public counter = 0;

    public addCount(){
        this.counter++;
    }
}