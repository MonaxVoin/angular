import { Pipe, PipeTransform } from "@angular/core";


@Pipe({
name: 'appCounterPipe'
})


export class AppCounterPipe  implements PipeTransform{
    transform(value: number){
        return value<100 ? 'Мало' : 'Много';

    }

}