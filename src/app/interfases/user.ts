import { StringifyOptions } from 'querystring';

export interface User {
  id: string;
  name: string;
  age: number;
  email: string;
  avatar?: string;
}



export interface BackendusersData {
  info: any;
  results: Result[];

}

export interface Result {
  cell: string;
  dob: Dob;
  email: string;
  gender: Gender;
  id: Id;
  location: Location;
  login: Login;
  name: Name;
  phone: string;
  picture: Picture;

}

interface Dob {
  age: number;
  date: string;
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female'
}

interface Id {
  name: string;
  value: string;
}


interface Location {
  city: string;
  coordinates: Coordinates;
  country: string;
  postcode: string;
  state: string;
  street: Street;

}


interface Street {
  number: number;
  name: string;
}


interface Login {
  password: string;
  username: string;
  uuid: string;
}

interface Name {
  first: string;
  last: string;
  title: Title;
}

export enum Title {
  MR = 'Mr',
  MS = 'Ms'
}

interface Picture {
  large: string;
  medium: string;
  thumbnail: string;
}
